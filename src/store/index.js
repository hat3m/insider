import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    selectedGrid : 5,
    mines: [],
    gameStatus: false,
    userName: '',
    isPlay: false,
    score: 0
  },
  getters: {
    
  },
  mutations: {
    
    changeGrid(state, selectedGrid) {
      state.selectedGrid = selectedGrid;
    },
    
    createMines(state, selectedGrid) {
      state.mines = [];
      let totalMine = selectedGrid*selectedGrid;
      for (let i = 0; i < totalMine; i++){
        state.mines.push({isMined:false, isSelected:false});
      }
      
      let minedCount = 0;
      let random = 0;
      
      while (minedCount < selectedGrid){
        random = Math.floor(Math.random() * totalMine);
        if(state.mines[random].isMined===false){
          state.mines[random].isMined=true;
          minedCount++;
        }
       }
    },
    
    startGame(state, status) {
      state.gameStatus = status;
      state.isPlay = status;
    },
    
    endGame(state) {
      state.isPlay = false;
    }
  },
  actions: {
  //  createMines({commit}, grid){
  //     commit('createMines', grid);
  //  }
  },
  modules: {

  }
})
